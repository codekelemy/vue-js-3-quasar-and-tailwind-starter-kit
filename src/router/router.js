// =======================================================================
// A P P   R O U T I N G 
// =======================================================================

import { createRouter, createWebHistory } from "vue-router"

const routes = [

    // =============================
    // INDEX
    // =============================
    {
        path: "/",
        name: "Index",
        component: () => import("@/views/Index.vue"),
    },
    
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
    scrollBehavior () {
        // always scroll to top
        return { top: 0 }
    }
});

export default router;
