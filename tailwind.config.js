module.exports = {
    purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {},
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            greeny: {
                light: '#4e9a3f',
                DEFAULT: '#137c3b',
                dark: '#0c682d',
            },
            golden: {
                light: '#f6cd51',
                DEFAULT: '#d4ab41',
                dark: '#ac832f',
            },
            lighty: {
                DEFAULT: '#eef4ef',
            }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
